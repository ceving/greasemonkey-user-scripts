// Copyright (C) 2017 Sascha Ziemann
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ==UserScript==
// @name        Wikipedia: Disable Fundraising
// @namespace   ceving
// @include     https://*.wikipedia.org/*
// @version     2
// @grant       none
// @updateURL   http://ceving.gitlab.io/greasemonkey-user-scripts/wikipedia_fundraising.user.js
// ==/UserScript==

(function() {
  var page = document.getElementById ('mw-page-base');
  var head = document.getElementById ('mw-head');
  var panel = document.getElementById ('mw-panel');
  
  var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      if (parseInt(window.getComputedStyle(page)['padding-top']) > 0) {
        document.getElementById ('centralNotice').style['display'] = 'none';
        page.style['padding-top'] = '0';
        head.style['top'] = '0';
        panel.style['top'] = '0';
      }
    })
  });

  observer.observe(page, {attributes: true, attributeFilter: ['style']});
})();
