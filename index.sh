#! /bin/bash

## Copyright (C) 2017 Sascha Ziemann
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

exec >index.html
index () {
  for f in $(printf "%s\n" *.js|sort); do
    n=$(sed -n 's/.*@name['$'\t'' ]\+//p' "$f")
    echo "<li><a href='$f'>$n</a></li>"
  done
}
declare -A TITLE=(
  [en]="User Scripts for Greasemonkey"
  [de]="Benutzer-Skripte für Greasemonkey"
)
cat <<EOF
<!DOCTYPE html>
<!--
Copyright (C) 2017 Sascha Ziemann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->
<html>
  <head>
    <title>${TITLE[en]}</title>
    <style>
[lang] {
  display: none;
}
[lang=en] {
  display: unset;
}
pre {
  margin: 0 0 0 1em;
  background: #eee;
  width: 72ch;
  height: 80vh;
  overflow: auto;
  padding: 1ex;
}
pre:empty {
  visibility: hidden;
}
ul {
  float: left;
  margin-right: 2em;
}
li {
  pointer-events: none;
}
li * {
  pointer-events: auto;
}
li > a + span {
  opacity: 0;
  padding-left: 1em;
  cursor: pointer;
}
li > a:hover + span, li > a + span:hover {
  opacity: 1;
}
li::after {
  content: '⟹';
  padding-left: 1em;
  opacity: 0;
  cursor: pointer;
  pointer-events: auto;
}
li.selected::after {
  opacity: 1;
}
    </style>
  </head>
  <body>
    <h1>
      <span lang="en">${TITLE[en]}</span>
      <span lang="de">${TITLE[de]}</span>
    </h1>
    <ul>
$(index)
    </ul>
    <pre></pre>
    <script>
function view_source(event) {
  fetch(event.target.previousSibling.href)
    .then(function(r) { return r.text(); })
    .then(function(text) {
      document.querySelector('pre').textContent = text; });
  document.querySelectorAll('li.selected').forEach(function(li) {
    li.classList.toggle('selected');
  });
  event.target.parentElement.classList.toggle('selected');
  event.stopPropagation();
}
function main() {
  document.querySelectorAll('li').forEach(function (li) {
    li.querySelectorAll('a').forEach(function (a) {
      var view = document.createElement('span');
      view.appendChild(document.createTextNode('🔍'));
      view.onclick = view_source;
      li.appendChild(view);
    });
    li.onclick = function (event) {
      li.classList.toggle('selected');
      document.querySelector('pre').textContent = '';
      event.stopPropagation();
    };
  });
  document.querySelectorAll('a').forEach(function(a) {
    a.onclick = function (event) {
      event.stopPropagation();
    };
  });
}
main();
function localize (language)
{
  if (['de'].includes(language)) {
    let lang = ':lang(' + language + ')';
    let hide = '[lang]:not(' + lang + ')';
    document.querySelectorAll(hide).forEach(function (node) {
      node.style.display = 'none';
    });
    let show = '[lang]' + lang;
    document.querySelectorAll(show).forEach(function (node) {
      node.style.display = 'unset';
    });
  }
}
localize(window.navigator.language);
    </script>
  </body>
</html>
EOF