// ==UserScript==
// @name         Telepolis
// @namespace    http://tampermonkey.net/
// @version      2
// @description  Werbung entfernen
// @author       You
// @match        https://www.heise.de/tp/*
// @grant        none
// @updateURL    http://ceving.gitlab.io/greasemonkey-user-scripts/telepolis.user.js
// ==/UserScript==

(function() {
   'use strict';

   // Werbe-Iframes entfernen
   document.querySelectorAll('iframe').forEach(function(node) {
     node.style.display = 'none';
   });
})();