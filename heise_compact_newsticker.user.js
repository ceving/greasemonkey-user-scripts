// Copyright (C) 2017,2018 Sascha Ziemann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ==UserScript==
// @name        Heise: Compact Newsticker
// @name:de     Heise: Kompakter Newsticker
// @namespace   ceving
// @include     https://www.heise.de/newsticker/
// @version     7
// @grant       GM_addStyle
// @updateURL   http://ceving.gitlab.io/greasemonkey-user-scripts/heise_compact_newsticker.user.js
// ==/UserScript==

// Abstände verringern, Trennlinien entfernen
document.querySelectorAll('a.archiv-liste__text').forEach(function(node) {
  node.style.marginLeft = 0;
  node.style.padding = 0;
  node.style.border = 'none';
  node.style.font_weight = 'initial';
});

document.querySelectorAll('section.newsticker-archiv__week').forEach(function(node) {
  node.style.marginBottom = '1rem';
});

// Artikel auf einer Seite anzeigen
document.querySelectorAll('a').forEach(function(node) {
  if (/meldung.*html$/.test(node.href)) {
    node.href = node.href + '?artikelseite=all';
  }
});

// H1 footer nach rechts floaten
GM_addStyle ( `
  footer.a-article-meta {
    opacity: 0.3;
    float: inline-end;
  }
` );

// Inline-Anzeigen löschen
document.querySelectorAll('li.archiv-liste__item--sponsored').forEach(function(node) {
  node.style.display = 'none';
});
