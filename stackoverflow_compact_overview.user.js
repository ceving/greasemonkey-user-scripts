// Copyright (C) 2017 Sascha Ziemann
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ==UserScript==
// @name        Stackoverflow: Compact Overview
// @name:de     Stackoverflow: Kompakte Übersicht
// @namespace   ceving
// @include     https://stackoverflow.com/*
// @version     3
// @grant       none
// @updateURL   http://ceving.gitlab.io/greasemonkey-user-scripts/stackoverflow_compact_overview.user.js
// ==/UserScript==

var observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    mutation.target.style.backgroundColor = 
      mutation.target.classList.contains('tagged-interesting') ? '#ffc' : 'initial';
  })
});

document.querySelectorAll('div.question-summary').forEach(function(node) {
  node.style.padding = '1ex 0px 0px 0px';
  node.style.border = 'none';
  observer.observe(node, {attributes: true, attributeFilter: ['class']});
});

document.querySelectorAll('div.summary').forEach(function(node) {

  node.querySelectorAll('h3').forEach(function(node) {
    node.style.margin = '0px';

    node.querySelectorAll('a.question-hyperlink').forEach(function(node) {
      node.style.fontSize = '16px';
    });
  });

  node.querySelectorAll('div.tags > a.post-tag').forEach(function(node) {
    node.style.fontSize = '10px';
    node.style.padding = '2pt';
    node.style.border = 'none';
  });
});

document.querySelectorAll('div.cp > div').forEach(function(node) {
  node.style.padding = '0px';
});