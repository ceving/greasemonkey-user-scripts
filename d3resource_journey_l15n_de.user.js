// Copyright (C) 2017 Sascha Ziemann
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ==UserScript==
// @name         German localization of d3resource.com/journey
// @name:de      Deutsche Lokalisierung für d3resource.com/journey
// @namespace    ceving
// @version      2
// @match        https://d3resource.com/journey/
// @grant        none
// @updateURL   http://ceving.gitlab.io/greasemonkey-user-scripts/d3resource_journey_l15n_de.user.js
// ==/UserScript==

(function() {
  'use strict';

  let de = {
    "td-sj1a": "Portal Normal",
    "td-sj2a": "Portal Profi",
    "td-sj3a": "Portal Meister",
    "td-sj4a": "Portal Qual&nbsp;1",
    "td-sj5a": "Portal Qual&nbsp;5",
    "td-sj6a": "Portal Qual&nbsp;10 in unter 6&nbsp;min",
    "td-sj7a": "Portal Qual&nbsp;12 in unter 6&nbsp;min",
    "td-sj8a": "Portal Qual&nbsp;13 in unter 5&nbsp;min",
    "td-sj9a": "Portal Qual&nbsp;13 in unter 4&nbsp;min",
    "td-sj1b": "Stufe&nbsp;50 erreichen",
    "td-sj2b": "Stufe&nbsp;70 erreichen",
    "td-sj3b": "Gegenstand bei Kadala kaufen",
    "td-sj4b": "Großes Portal Stufe&nbsp;20 solo",
    "td-sj5b": "Großes Portal Stufe&nbsp;30 solo",
    "td-sj6b": "Großes Portal Stufe&nbsp;40 solo",
    "td-sj7b": "Großes Portal Stufe&nbsp;50 solo",
    "td-sj8b": "Großes Portal Stufe&nbsp;60 solo",
    "td-sj9b": "Großes Portal Stufe&nbsp;70 solo",
    "td-sj1c": "Malthael besiegen",
    "td-sj2c": "Diablo besiegen (Schwer, Stufe&nbsp;60+)",
    "td-sj3c": "Rakanoth besiegen (Meister, Stufe&nbsp;70)",
    "td-sj4c": "Schlächter besiegen (Qual 2, Stufe&nbsp;70)",
    "td-sj5c": "Krull besiegen (Qual&nbsp;7)",
    "td-sj6c": "Cydaea besiegen (Qual&nbsp;10)",
    "td-sj7c": "Maghda besiegen (Qual&nbsp;13)",
    "td-sj8c": "Araneae besiegen (Qual&nbsp;13) in unter 30&nbsp;s",
    "td-sj9c": "Skelettkönig besiegen (Qual&nbsp;13) in unter 15&nbsp;s",
    "td-sj1d": "Azmodan besiegen",
    "td-sj2d": "Belagerungs&shy;brecher besiegen (Schwer, Stufe&nbsp;60+)",
    "td-sj3d": "Izual besiegen (Meister, Stufe&nbsp;70)",
    "td-sj4d": "Urzael besiegen (Qual&nbsp;4, Stufe&nbsp;70)",
    "td-sj5d": "Ghom besiegen (Qual&nbsp;7)",
    "td-sj6d": "Adria besiegen (Qual&nbsp;10)",
    "td-sj7d": "Belial besiegen (Qual&nbsp;13)",
    "td-sj8d": "Greed besiegen (Qual&nbsp;13)",
    "td-sj2e": "Kanai's Würfel finden",
    "td-sj3e": "Eine Legendäre Eigenschaft extrahieren",
    "td-sj4e": "Selten Gegenstand zu legendärem aufwerten",
    "td-sj5e": "Würfel vollständig ausrüsten",
    "td-sj6e": "Set-Gegenstand umwandeln",
    "td-sj7e": "20&nbsp;legendäre Eigenschaften extrahieren",
    "td-sj8e": "Legendären Gegenstand umwandeln",
    "td-sj9e": "40&nbsp;legendäre Eigenschaften extrahieren",
    "td-sj1f": "5&nbsp;Kopfgelder",
    "td-sj2f": "Einen Begleiter vollständig ausrüsten",
    "td-sj3f": "Imperialen Edelstein erzeugen",
    "td-sj4f": "Gegenstand sockeln",
    "td-sj5f": "Legendären Gegenstand von Kadala erhalten",
    "td-sj6f": "Makellos königlichen Edelstein herstellen",
    "td-sj8f": "Gegenstand mit Stufe&nbsp;50 Juwel verstärken",
    "td-sj1g": "5&nbsp;Sockel mit Edelsteinen füllen",
    "td-sj2g": "Alle Handwerker auf Stufe&nbsp;12 ausbilden",
    "td-sj3g": "Horadischer Würfel aus Akt&nbsp;1",
    "td-sj4g": "Schlüssel&shy;hüter in Akt&nbsp;1 (Qual&nbsp;4)",
    "td-sj5g": "Ein Juwel auf Stufe&nbsp;25 aufwerten",
    "td-sj6g": "Drei Juwelen auf Stufe&nbsp;35 aufwerten",
    "td-sj7g": "Drei Juwelen auf Stufe&nbsp;45 aufwerten",
    "td-sj8g": "Drei Juwelen auf Stufe&nbsp;55 aufwerten",
    "td-sj9g": "Drei Juwelen auf Stufe&nbsp;70 aufwerten",
    "td-sj1h": "Schmied auf Stufe&nbsp;10 ausbilden",
    "td-sj2h": "Stufe&nbsp;70 Gegenstand beim Schmied erstellen",
    "td-sj3h": "Horadischer Würfel aus Akt&nbsp;2",
    "td-sj4h": "Schlüssel&shy;hüter in Akt&nbsp;2 (Qual&nbsp;4)",
    "td-sj5h": "Set Dungeon abschließen",
    "td-sj6h": "Set Dungeon meistern",
    "td-sj7h": "Eine Errungen&shy;schaft",
    "td-sj8h": "Zwei Errungen&shy;schaften",
    "td-sj9h": "Drei Errungen&shy;schaften",
    "td-sj1i": "Juwelier auf Stufe&nbsp;10 ausbilden",
    "td-sj2i": "Stufe&nbsp;70 Gegenstand beim Juwelier erzeugen",
    "td-sj3i": "Horadischer Würfel aus Akt&nbsp;3",
    "td-sj4i": "Schlüssel&shy;hüter in Akt&nbsp;3 (Qual&nbsp;4)",
    "td-sj5i": "(A1) Realm of Regret (Qual 1)",
    "td-sj6i": "(A3) Realm of Terror (Qual 10)",
    "td-sj7i": "Höllenfeuer-Amulet erschaffen",
    "td-sj1j": "Mystikerin auf Stufe&nbsp;10 ausbilden",
    "td-sj2j": "Verzaubern bei der Mystikerin",
    "td-sj3j": "Horadischer Würfel aus Akt&nbsp;4",
    "td-sj4j": "Schlüssel&shy;hüter in Akt&nbsp;4 (Qual&nbsp;4)",
    "td-sj5j": "(A2) Realm of  Putridness (Qual&nbsp;1)",
    "td-sj6j": "(A4) Realm of Fright (Qual&nbsp;10)",
    "td-sj2k": "Trans&shy;mo&shy;di&shy;fi&shy;zieren bei der Mystikerin",
    "td-sj3k": "Horadischer Würfel aus Akt&nbsp;5",
    "td-sj4k": "Juwelier 5&nbsp;Rezepte beibringen",
    "td-sj4l": "Schmied 5&nbsp;Rezepte beibringen"
  };

  let t = document.querySelector('table.sjtablelayout');
  t.style['table-layout'] = 'fixed';
  t.style['width'] = '100%';
  for (let id in de) {
    let el = document.getElementById(id);
    el.innerHTML = de[id];
    el.style['white-space'] = 'normal';
  }
})();
