// Copyright (C) 2017 Sascha Ziemann
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ==UserScript==
// @name        Google Groups: Disable translate spam
// @namespace   ceving
// @include     https://groups.google.com/*
// @version     2
// @grant       none
// @updateURL   http://ceving.gitlab.io/greasemonkey-user-scripts/google-groups_no-translate.user.js
// ==/UserScript==

(function() {
  var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      document.body.querySelectorAll('.F0XO1GC-nb-lb').forEach(function(node) {
        node.style.display = 'none';
      });
    });
  });

  observer.observe(document.body, {childList: true});
})();
